<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Classes\Commander;
use BinaryStudioAcademy\Game\Classes\Galaxy;
use BinaryStudioAcademy\Game\Classes\PatrolSpaceship;
use BinaryStudioAcademy\Game\Classes\PlayerSpaceship;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class Game
{
    private $random;
    private $commander;
    public static $playrShip;

    public function __construct(Random $random)
    {
        $this->random = $random;
        $this->commander = new Commander();
        self::$playrShip = new PlayerSpaceship();

    }

    public function start(Reader $reader, Writer $writer)
    {

        for($i = 0; ; $i++) {
            $command = trim($reader->read());
            if ($command === Commander::EXIT_COMMAND) {
                break;
            }

            switch ($command) {
                case Commander::HELP_COMMAND:
                case Commander::STATS:
                    $writer->writeln($this->commander->$command());
                    break;
                case Commander::ATTACK_COMMAND:
                    $writer->writeln($this->commander->$command(self::$playrShip, (new  Galaxy(''))->getShip()));
                    break;
            }

        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $writer->writeln('This method runs program step by step.');
    }
}
