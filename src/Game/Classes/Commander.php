<?php


namespace BinaryStudioAcademy\Game\Classes;

use BinaryStudioAcademy\Game\Game;

class Commander
{
    public const EXIT_COMMAND = 'exit';
    public const HELP_COMMAND = 'help';
    public const ATTACK_COMMAND = 'attack';
    public const STATS = 'stats';

    private $allowedCommands = [
        self::HELP_COMMAND,
        self::ATTACK_COMMAND,
        self::EXIT_COMMAND,
        self::STATS,
    ];


    public function help(): string
    {
        return 'List of commands:' . PHP_EOL
            . 'help - shows this list of commands' . PHP_EOL
            . 'stats - shows stats of spaceship' . PHP_EOL
            . 'set-galaxy <home|andromeda|spiral|pegasus|shiar|xeno|isop> - provides jump into specified galaxy' . PHP_EOL
            . 'attack - attacks enemy\'s spaceship' . PHP_EOL
            . 'grab - grab useful load from the spaceship' . PHP_EOL
            . 'buy <strength|armor|reactor> - buys skill or reactor (1 item)' . PHP_EOL
            . 'apply-reactor - apply magnet reactor to increase spaceship health level on 20 points' . PHP_EOL
            . 'whereami - shows current galaxy' . PHP_EOL
            . 'restart - restarts game' . PHP_EOL
            . 'exit - ends the game' . PHP_EOL;
    }

    public function stats(): string
    {
        return 'Spaceship stats:' . PHP_EOL
            . 'strength: ' . Game::$playrShip->getStrength() . PHP_EOL
            . 'armor: ' . Game::$playrShip->getArmor() . PHP_EOL
            . 'luck: ' . Game::$playrShip->getLuck() . PHP_EOL
            . 'health: ' . Game::$playrShip->getHealth(). PHP_EOL
            . 'hold: ' . (empty(Game::$playrShip->getHold()) ? '[ _ _ _ ]' : Game::$playrShip->getHold()). PHP_EOL;
    }

    public function attack(Ship $playerShip, Ship $enemyShip)
    {
        // TODO
        return 'attack is end';
    }

}