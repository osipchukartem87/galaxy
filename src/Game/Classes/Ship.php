<?php


namespace BinaryStudioAcademy\Game\Classes;



abstract class Ship
{
    protected int $strength = 0;
    protected int $armor = 0;
    protected int $luck = 0;
    protected int $health = 0;
    protected array $hold = [];
    protected int $minStrength = 0;
    protected int $maxStrength = 0;
    protected int $minArmor = 0;
    protected int $maxArmor = 0;
    protected int $minLuck = 0;
    protected int $maxLuck = 0;

    public function __construct(){
        $this->strength = $this->strength != null ? $this->getStrength() : rand($this->minStrength, $this->maxStrength);
        $this->armor = $this->armor != null ? $this->getArmor() : rand($this->minArmor, $this->maxArmor);
        $this->luck = $this->luck != null ? $this->getLuck() : rand($this->minLuck, $this->maxLuck);
    }

    abstract public function setStrength($strength): void;
    abstract public function setArmor($armor): void;
    abstract public function setHealth($health): void;
    abstract public function setHold($hold) : void;

    abstract public function getStrength(): int;
    abstract public function getArmor(): int;
    abstract public function getLuck(): int;
    abstract public function getHealth(): int;
    abstract public function getHold() : array;

}
