<?php


namespace BinaryStudioAcademy\Game\Classes;


class Galaxy
{
    protected $ship;

    public function __construct(string $galaxyName) {
        switch($galaxyName) {
            case 'home':
                 return $this->setShip(new PlayerSpaceship());
            case 'andromeda':
            case 'pegasus':
            case 'spiral':
                return $this->setShip(new PatrolSpaceship());
            case 'shiar':
            case 'xeno':
                return $this->setShip(new BattleSpaceship());
            case'isop':
                return $this->setShip(new Executor());
            
            default: throw new Exception('Unknown type!!!');

        }
    }

    protected function setShip (Ship $ship) {
        $this->ship = $ship;
    }

    public function getShip() {
        return $this->ship;
    }

}
