<?php


namespace BinaryStudioAcademy\Game\Classes;


class PlayerSpaceship extends Ship
{
	protected int $strength = 5;
    protected int $armor = 5;
    protected int $luck = 5;
    protected int $health = 100;
    protected array $hold = [];

    public function setStrength($strength): void
    {
        $this->strength = $strength;
    }

    public function setArmor($armor): void
    {
        $this->armor = $armor;
    }

    public function setHealth($health): void
    {
        $this->health = $health;
    }

    public function setHold($hold): void
    {
        $this->hold = $hold;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getArmor(): int
    {
        return $this->armor;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getHold(): array
    {
        return $this->hold;
    }

}
