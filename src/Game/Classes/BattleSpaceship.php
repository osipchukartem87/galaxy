<?php


namespace BinaryStudioAcademy\Game\Classes;


class BattleSpaceship extends Ship
{
    protected int $strength = 0;
    protected int $maxStrength = 8;
    protected int $minStrength = 5;
    protected int $armor = 0;
    protected int $maxArmor = 8;
    protected int $minArmor = 6;
    protected int $luck = 0;
    protected int $maxLuck = 6;
    protected int $minLuck = 3;
    protected int $health = 100;
    protected array $hold = ['green', 'purple'];

    public function setStrength($strength): void
    {
        $this->strength = $strength;
    }

    public function setArmor($armor): void
    {
        $this->armor = $armor;
    }

    public function setHealth($health): void
    {
        $this->health = $health;
    }

    public function setHold($hold): void
    {
        $this->hold = $hold;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getArmor(): int
    {
        return $this->armor;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getHold(): array
    {
        return $this->hold;
    }

}
